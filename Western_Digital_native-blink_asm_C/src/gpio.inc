# SPDX-License-Identifier: Unlicense
# Copyright (c) 2018 Western Digital Corporation or its affiliates.

# Address offsets to GPIO_CTRL_ADDR:
.equ GPIO_OUTPUT_EN,    0x008
.equ GPIO_OUTPUT_VAL,   0x00C
.equ GPIO_OUTPUT_XOR,   0x040

# SiFive HiFive1 board: RGB LED uses GPIO pins 22, 19, 21.
# Value bits:
# .equ GPIO_RGB_PINS,     0x680000
# .equ GPIO_RED_LED,      0x400000
# .equ GPIO_BLUE_LED,     0x200000
# .equ GPIO_GREEN_LED,    0x080000

# SparkFun RED-V RedBoard / thing plus are using GPIO pin 5 for blue LED.
.equ GPIO_RGB_PINS,     0x400030
.equ GPIO_RED_LED,      0x400000
.equ GPIO_BLUE_LED,     0x000020
.equ GPIO_GREEN_LED,    0x000010
