## Switch ON all GPIO outputs

Pure Assembly language version.

### How to deploy

Install PlatformIO:

```sh
python3 -m venv --system-site-packages /tmp/venv_platformio
source /tmp/venv_platformio/bin/activate
pip3 install platformio
```

Build and flash:
```sh
pio run --target upload
```

### TODO

Remove `framework = freedom-e-sdk` from `platformio.ini`. Curreny I can't do it, because PlatformIO can't flash the code with J-Link:
