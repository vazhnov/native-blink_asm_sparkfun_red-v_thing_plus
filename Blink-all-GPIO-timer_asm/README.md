## Blink with all GPIO outputs

**Doesn't work yet! See [SiFive FE310-G002: Using WFI together with MTIMECMP](https://forums.sifive.com/t/sifive-fe310-g002-using-wfi-together-with-mtimecmp/6365).**

Pure Assembly language version.

Uses timer + interrupts + halt (`WFI`) for delay, instead of loop, to save some evergy.

Independent of CPU frequency, uses RTC 32768 Hz crystal (MTIME register `0x0200BFF8`).

### How to deploy

Install PlatformIO:

```sh
python3 -m venv --system-site-packages /tmp/venv_platformio
source /tmp/venv_platformio/bin/activate
pip3 install platformio
```

Build and flash:
```sh
pio run --target upload
```

## Links

Some useful links:
* https://forums.sifive.com/t/beginner-trying-to-set-up-timer-irq-in-assembler-how-to-print-csrs-in-gdb/2764/26 , which links to https://sillymon.ch/cgit/freedom-e-sdk/tree/software/first?h=useTimerIRQ
* [How to enable rtc in fe310 with 1sec resolution in rtcs?](https://forums.sifive.com/t/how-to-enable-rtc-in-fe310-with-1sec-resolution-in-rtcs/6289).
* https://stackoverflow.com/questions/59656763
