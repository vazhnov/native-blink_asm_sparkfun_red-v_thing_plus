// SPDX-License-Identifier: Unlicense
// Copyright (c) 2018 Western Digital Corporation or its affiliates.

#include <stdio.h>

void setupGPIO();

int main()
{
    printf("Example 'Switch-on-all-GPIO_asm_C' for SparkFun RED-V board\r\n");
    printf("From ssh://git@gitlab.com/vazhnov/native-blink_asm_sparkfun_red-v_thing_plus.git\r\n");
    printf("v0.2\r\n");

    printf("Setup GPIO:\r\n");
    setupGPIO();
    printf("Done! (should never be printed)\r\n");
}
