## Switch ON all GPIO outputs

C + Assembly languages version.

### How to deploy

Install PlatformIO:

```sh
python3 -m venv --system-site-packages /tmp/venv_platformio
source /tmp/venv_platformio/bin/activate
pip3 install platformio
```

Build and flash:
```sh
pio run --target upload
```
