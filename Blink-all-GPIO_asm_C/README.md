## Blink with all GPIO outputs

C + Assembly languages version.

Power-hungry: uses loop for delay.

Independent of CPU frequency, uses RTC 32768 Hz crystal (MTIME register `0x0200BFF8`).

### How to deploy

Install PlatformIO:

```sh
python3 -m venv --system-site-packages /tmp/venv_platformio
source /tmp/venv_platformio/bin/activate
pip3 install platformio
```

Build and flash:
```sh
pio run --target upload
```

## TODO

Use timer + interrupts + halt (`WFI`) for delay, instead of loop. Example: https://stackoverflow.com/questions/59656763
