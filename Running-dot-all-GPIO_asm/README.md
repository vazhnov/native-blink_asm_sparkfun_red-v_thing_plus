## Blink with all GPIO outputs

Pure Assembly language version.

Power-hungry: uses loop for delay.

Independent of CPU frequency, uses RTC 32768 Hz crystal (MTIME register `0x0200BFF8`).

As an experiment, this code example created without using framework [freedom-e-sdk](https://registry.platformio.org/tools/platformio/framework-freedom-e-sdk), so a linker configuration was added from https://github.com/psherman42/Demystifying-OpenOCD/blob/main/fe310-g002-rom.lds . See [community.platformio.org](https://community.platformio.org/t/cant-upload-with-j-link-to-sifive-fe310-when-framework-is-empty/37622) thread for more details.

### How to deploy

Install PlatformIO:

```sh
python3 -m venv --system-site-packages /tmp/venv_platformio
source /tmp/venv_platformio/bin/activate
pip3 install platformio
```

Build and flash:
```sh
pio run --target upload
```

## TODO

Use timer + interrupts + halt (`WFI`) for delay, instead of loop. Example: https://stackoverflow.com/questions/59656763
